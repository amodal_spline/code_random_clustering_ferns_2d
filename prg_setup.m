%% Setup
% 
% Description:
%   This function adds the program paths and compiles the mex files.
%
% Contact:
%   Michael Villamizar
%   mvillami-at-iri.upc.edu
%   Institut de Robòtica i Informática Industrial CSIC-UPC
%   Barcelona - Spain
%   2014
%

%% Main function
function prg_setup()
clc,close all,clear all

% message
fun_messages('Program Setup','presentation');
fun_messages('Setup','title');

% root path
fun_messages('adding program paths','process');
[root,~,~] = fileparts(mfilename('fullpath'));

% download pLSA code from Visual Geometry Group - University of Oxford
url = 'http://www.robots.ox.ac.uk/~vgg/software/pLSA/pLSA_demo.tgz';
fun_messages('downloading pLSA code','information');
untar(url,'./tools/pLSA_demo')

% add paths: root and mex files
fun_messages('adding root path','information');
addpath(root);
fun_messages('adding files path','information');
addpath(fullfile(root,'/files'));
fun_messages('adding functions files path','information');
addpath(fullfile(root,'/files/functions'));
fun_messages('adding PLSA files path','information');
addpath(fullfile(root,'/tools/pLSA_demo'));

% root path
cd(root);

% message
fun_messages('end','title');

end

%% messages
% This function prints a specific message on the command window
function fun_messages(text,message)
if (nargin~=2), error('incorrect input parameters'); end

% types of messages
switch (message)
    case 'presentation'
        fprintf('****************************************************\n');
        fprintf(' %s\n',text);
        fprintf('****************************************************\n');
        fprintf(' Michael Villamizar\n mvillami@iri.upc.edu\n');
        fprintf(' http://www.iri.upc.edu/people/mvillami/\n');
        fprintf(' Institut de Robòtica i Informàtica Industrial CSIC-UPC\n');
        fprintf(' c. Llorens i Artigas 4-6\n 08028 - Barcelona - Spain\n 2016\n');
        fprintf('****************************************************\n\n');
    case 'title'
        fprintf('****************************************************\n');
        fprintf('%s\n',text);
        fprintf('****************************************************\n');
    case 'process'
        fprintf('-> %s\n',text);
    case 'information'
        fprintf('->     %s\n',text);
    case 'warning'
        fprintf('-> %s !!!\n',text);
    case 'error'
        fprintf(':$ ERROR : %s\n',text);
        error('program error');
end
end

