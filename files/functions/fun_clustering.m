%% Clustering
% This function performs clustering on the input samples.
function output = fun_clustering(classifier,samples)

% probabilistic latent semantic analysis
clusters = fun_clustering_plsa(classifier,samples);

% output
output = clusters;
end

%% Clustering: plsa
% This function performs clustering in the input samples using probabilistic 
% latent semantic analysis.
function output = fun_clustering_plsa(classifier,samples)
if (nargin~=2), fun_messages('incorrect input variables','error'); end

% parameters
prms = fun_parameters();  % program parameters
minLog = prms.clustering.minLog;  % EM stopping condition: min. log-likelihood
showPlots = prms.clustering.showPlots;  % ploting verbosity
numClusters = prms.clustering.numClusters;  % num. clusters
numMaxIters = prms.clustering.numMaxIters;  % maximum number of iterations of EM

% num. ferns, features and fern outputs (histogram bins)
numFerns = size(classifier.ferns,1);
numFeats = size(classifier.ferns,2);
numBins = 2^numFeats;

% num. samples
numSamples = size(samples,1);

% messages
fun_messages('clustering: plsa','process');
fun_messages(sprintf('num. clusters: %d',numClusters),'information');
fun_messages(sprintf('num. samples: %d',numSamples),'information');
fun_messages(sprintf('num. max. iterations: %d',numMaxIters),'information');
fun_messages(sprintf('min. log: %.3f',minLog),'information');

% allocate
data = zeros(numSamples,numBins*numFerns);  % visual words data

% variables
eps = 0.000001;

% visual words on the input samples
for iterSample = 1:numSamples
	% sample
	sample = samples(iterSample,:);
	% fern output values
	values = fun_fern_outputs(sample,classifier.ferns);
	% visual words vector
	words = fun_visual_words(values,numBins);
	% save data
	data(iterSample,:) = words;
end

% clustering via probabilitic latent semantic analysis (plsa)
plsaPrms.Leps = minLog; % EM stopping condition: minimum change in log-likelihood
plsaPrms.doplot = showPlots; % ploting verbosity
plsaPrms.maxit = numMaxIters; % maximum number of iterations of EM
[Pw_t,Px_t,Pt,Li] = pLSA_EM(data'+eps,numClusters,plsaPrms);

% plsa data
clusters.Pt = Pt;
clusters.Pw_t = Pw_t;
clusters.Px_t = Px_t;

% output
output = clusters;
end
