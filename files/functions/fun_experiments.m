%% Experiments
% This file contains the experiments to conduct where
% each one is devoted to evaluate a particular configuration
% of random clustering ferns.
function output = fun_experiments()

% global variables
global iterExp;

% experiments
switch (iterExp)
case 1
	data.tag = 'exp_t1';
	data.numClusters = 5;
	data.numFerns = 50;
	data.numFeats = 5;
otherwise
	error('no more experiments');
end

% output
output = data;
end
