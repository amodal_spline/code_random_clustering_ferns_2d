%% Save data
% This function saves program data as *.mat file.
function fun_data_save(data,path,name)
if (nargin~=3), fun_messages('incorrect input parameters','error'); end

% save data
save([path,name],'data');

end
