%% Parameters
% This function contains the program parameters.
function output = fun_parameters()
if (nargin~=0), fun_messages('incorrect input parameters','error'); end

% experiment parameters
prmsExp = fun_experiments();

% samples
numDims = 2;  % num. feature space dimensions (2 by default)
clusThr = 0.13;  % cluster distance threshold
clusStd = 0.04;  % cluster standard deviation
numClusters = prmsExp.numClusters;  % num. positive classes
numPosSamples = 1000;  % num. positive class samples
numNegSamples = 1000;  % num. negative class samples
samples = struct('numClusters',numClusters,'numPosSamples',numPosSamples,...
	'numNegSamples',numNegSamples,'clusStd',clusStd,'numDims',...
	numDims,'clusThr',clusThr);

% classifier
numFerns = prmsExp.numFerns;  % num. random ferns
numFeats = prmsExp.numFeats;  % num. features
poolSize = 1000;  % pool size -sets of random ferns-
classifier = struct('poolSize',poolSize,'numFerns',numFerns,'numFeats',...
	numFeats);

% clustering
minLog = 0.1;  % plsa: EM stopping condition: minimum change in log-likelihood
showPlots = 0;  % plsa: ploting verbosity
numClusters = prmsExp.numClusters;  % num. clusters
numMaxIters = 100;  % plsa: maximum number of iterations of EM
clustering = struct('showPlots',showPlots,'numMaxIters',numMaxIters,...
	'numClusters',numClusters,'minLog',minLog);

% visualization
distSize = 100;  % distribution size -num. bins-
posColor = [1,0.7,0];  % color for positive samples
negColor = [0,0,1];  % color for negative samples
gridSize = 100;  % spatial grid size
fontSize = 14;  % font size
lineWidth = 4;  % line width
markerSize = 8;  % marker size
visualization = struct('markerSize',markerSize,'lineWidth',lineWidth,...
	'fontSize',fontSize,'gridSize',gridSize,'posColor',posColor,...
	'negColor',negColor,'distSize',distSize);

% parameters
prms.samples = samples;
prms.clustering = clustering;
prms.classifier = classifier;
prms.visualization = visualization;

% output
output = prms;
end
