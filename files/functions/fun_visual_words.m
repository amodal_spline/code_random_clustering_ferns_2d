%% Visual word vector
% This function computes a vector including visual words using the fern
% outputs over an input sample (computed in advance).
function output = fun_visual_words(fernOutputs,numBins)
if (nargin~=2), fun_messages('incorrect input variables','error'); end

% num. ferns
numFerns = size(fernOutputs,2);

% variables
words = zeros(1,numBins*numFerns);

% words
for iter = 1:numFerns

   % fern output 
   z = fernOutputs(1,iter);
   
   % save
   index = (iter-1)*numBins + z;
   words(1,index) = 1;
    
end

% output
output = words;
end
