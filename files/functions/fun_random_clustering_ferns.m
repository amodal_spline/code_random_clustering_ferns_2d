%% Random clustering ferns
% This function computes the random clustering ferns (rcfs). The rcfs combine
% the boosted random classifier (two-class classifier) and probabilistic latent
% semantic analysis to classify and clustering the input samples.
function output = fun_random_clustering_ferns(samples,labels,means)
if (nargin~=3), fun_messages('incorrect input variables','error'); end

% parameters
prms = fun_parameters();  % program parameters
poolSize = prms.classifier.poolSize;  % num. binary feature sets
numFeats = prms.classifier.numFeats;  % num. binary features

% fern pool: random sets of binary features -decision stumps-
pool = fun_fern_pool(poolSize,numFeats);

% two-class classifier
classifier = fun_classifier(pool,samples,labels);

% clustering
clusters = fun_clustering(classifier,samples);

% rcfs
rcfs.prms = prms;
rcfs.clusters = clusters;
rcfs.classifier = classifier;

% output
output = rcfs;
end

%% Fern pool
% This function computes a random pool with random ferns where each one has
% several binary features -decision stumps-.
function output = fun_fern_pool(poolSize,numFeats)
if (nargin~=2), fun_messages('incorrect input variables','error'); end

% parameters
prms = fun_parameters();  % parameters
numDims = prms.samples.numDims;  % num. feature space dimensions

% allocate
pool = zeros(poolSize,numFeats,2);

% pool
for iterPool = 1:poolSize
	for iterFeat = 1:numFeats
		% feature: axis (xj) and threshold (tau)
		x = min(numDims,ceil(rand*numDims));
		t = rand;
		% save
		pool(iterPool,iterFeat,1) = x;
		pool(iterPool,iterFeat,2) = t;
	end
end

% output
output = pool;
end

%% Classifier
% This function computes the boosted random ferns classifier.
function output = fun_classifier(pool,samples,labels)
if (nargin~=3), fun_messages('incorrect input variables','error'); end

% class indexes
posIndxs = labels>0;
negIndxs = labels<0;

% positive and negative samples
posSamples = samples(posIndxs,:);
negSamples = samples(negIndxs,:);

% classifier: boosted random ferns
classifier = fun_classifier_brfs(pool,posSamples,negSamples);

% output
output = classifier;
end

%% Classifier: boosted random ferns
% This function computes a boosted random ferns classifier using the input
% samples and fern pool.
function output = fun_classifier_brfs(pool,posSamples,negSamples)
if (nargin~=3), fun_messages('incorrect input variables','error'); end

% message
fun_messages('classifier: boosted random ferns -brfs-','process');

% parameters
prms = fun_parameters();  % program parameters
poolSize = prms.classifier.poolSize;  % pool size
numFerns = prms.classifier.numFerns;  % num. random ferns
numFeats = prms.classifier.numFeats;  % num. binary features -decision stumps-
numBins  = 2^numFeats;  % num. histogram bins

% variables
eps = 0.001;  % epsilon

% num. positive and negative samples
numPosSamples = size(posSamples,1);
numNegSamples = size(negSamples,1);

% num. samples
numSamples = numPosSamples + numNegSamples;

% labels
labels = [ones(1,numPosSamples),-1*ones(1,numNegSamples)];

% sample weights
weights = (1/numSamples)*ones(1,numSamples);

% allocate
ferns = zeros(numFerns,numFeats,2);  % boosted ferns data -binary features-
hstms = zeros(numFerns,numBins);  % boosted fern probabilities -histograms-
indexes = zeros(1,numFerns);  % weak classifier indexes
posMaps = zeros(numPosSamples,poolSize);  % fern output maps on positive samples
negMaps = zeros(numNegSamples,poolSize);  % fern output maps on negative samples

% ferns outputs on positive samples
for iter = 1:numPosSamples
	posMaps(iter,:) = fun_fern_outputs(posSamples(iter,:),pool);
end

% ferns outputs on negative samples
for iter = 1:numNegSamples
	negMaps(iter,:) = fun_fern_outputs(negSamples(iter,:),pool);
end

% boosting
for iterFern = 1:numFerns
    
    % variables
    Qmax = inf;  % max. Bhattacharyya distance
    hstm = zeros(1,numBins);  % fern distribution
    conf = zeros(1,numSamples);  % confidence
    
    % fern pool
    for iterPool = 1:poolSize
        
        % variables
        posHstm = zeros(1,numBins);  % positive fern distribution
        negHstm = zeros(1,numBins);  % negative fern distribution
        
        % positive fern distribution
        for iterSample = 1:numPosSamples
            z = posMaps(iterSample,iterPool);
            posHstm(1,z) = posHstm(1,z) + weights(1,iterSample);
        end
        
        % negative fern distribution
        for iterSample = 1:numNegSamples
            z = negMaps(iterSample,iterPool);
            negHstm(1,z) = negHstm(1,z) + weights(1,iterSample + numPosSamples);
        end
        
        % normalization
        posHstm = posHstm./sum(posHstm(:));
        negHstm = negHstm./sum(negHstm(:));
        
        % Bhattacharyya distance
        Q = 2*sum(sqrt((posHstm.*negHstm)),2);
        
        % best fern
        if (Q<Qmax) && (sum(iterPool==indexes,2)==0)
            % update
            Qmax = Q;  % Bhattacharyya distance
            hstm = 0.5*log((posHstm + eps)./(negHstm + eps));  % log. ratio fern distribution
            index = iterPool;  % fern index
        end
    end
    
    % check
    if (Qmax==inf), fun_messages('not found weak classifier','error'); end
    
    % weak classifier (fern) confidence on positive samples
    for iterSample = 1:numPosSamples
        z = posMaps(iterSample,index);
        conf(1,iterSample) = hstm(1,z);
    end
    
    % weak classifier (fern) confidence on negative samples
    for iterSample = 1:numNegSamples
        z = negMaps(iterSample,index);
        conf(1,iterSample + numPosSamples) = hstm(1,z);
    end
    
    % update weights
    weights = weights.*exp(-1*labels.*conf);
    
    % normalization
    weights = weights./(sum(weights(:)));
    
    % update fern indexes
    indexes(1,iterFern) = index;
    
    % save data
    hstms(iterFern,:) = hstm;  % fern distribution
    ferns(iterFern,:,:) = pool(index,:,:);  % fern data
    
    % message
    if (mod(iterFern,10)==0), fun_messages(sprintf('fern: %d/%d',...
            iterFern,numFerns),'information'); end
end

% classifier -brfs-
classifier.ferns = ferns;  % random ferns data
classifier.hstms = hstms;  % ferns distributions

% output
output = classifier;
end
