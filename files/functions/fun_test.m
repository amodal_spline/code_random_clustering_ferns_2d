%% Test
% This function tests the random clustering ferns on the input samples in order
% to measure its classification performance. 
function output = fun_test(rcfs,samples,labels)
if (nargin~=3), fun_messages('incorrect input variables','error'); end

% num. samples and clusters
numSamples = size(samples,1);
numClusters = rcfs.prms.samples.numClusters;

% Sample scores: The random clustering ferns are tested on the input samples to
% compute the sample scores or classifier confidence over these samples.
[scores,estLabels] = fun_test_rcfs(rcfs,samples);

% positive and negative scores
posScores = scores(find(labels>0));
negScores = scores(find(labels<0));

% score -gaussian- distributions
posMiu = mean(posScores);  % positive class mean
negMiu = mean(negScores);  % negative class mean
posVar = std(posScores)^2;  % positive class variance
negVar = std(negScores)^2;  % negative class variance

% Score distribution distances: This computes the distance between the positive
% and negative score distributions, indicating the degree of separability 
% between these two classes.
dst = fun_score_distributions_distance(posMiu,negMiu,posVar,negVar);

% Classification rates: This function computes some classification rates of the
% classifier over the input samples (scores,labels). In detail, the function 
% computes the recall, precision and f-measure for different classifier 
% thresholds. The function also returns the equal error rate (EER), that point
% where precision=recall, and the classifier thresholds where EER is attained.
rates = fun_classification_rates(scores,labels);

% update estimated labels -negative labels- using the classifier threshold
indx = find(scores<=rates.thr);
estLabels(indx) = -1;

% Confusion matrix: This function computes the confusion matrix for the intra-
% class clusters labels and the entropy of this matrix as a metric to express
% the clustering performance.
[cnfMat,cnfEnt] = fun_confusion_matrix(labels,estLabels,numClusters);

% classification results
results.eer = rates.eer;  % equal error rate (EER)
results.thr = rates.thr;  % classification threshold to obtain EER
results.idx = rates.idx;  % threshold index -EER index-
results.dst = dst;  % distribution distance
results.lbl = labels;  % true sample labels
results.est = estLabels;  % estimated sample labels
results.cnfMat = cnfMat;  % confusion matrix
results.cnfEnt = cnfEnt;  % entropy metric (confusion matrix)
results.samples = samples;  % samples
results.curves.rec = rates.rec;  % recall curve
results.curves.pre = rates.pre;  % precision curve
results.curves.fme = rates.fme;  % f-measure curve
results.curves.tps = rates.tps;  % true positives curve
results.curves.fps = rates.fps;  % false positives curve
results.curves.fns = rates.fns;  % false negatives curve
results.distrs.posMiu = posMiu;  % positive class mean
results.distrs.negMiu = negMiu;  % negative class mean
results.distrs.posVar = posVar;  % positive class variance
results.distrs.negVar = negVar;  % negative class variance
results.scores.samples = scores; % classification scores
results.scores.positive = posScores;  % positive scores
results.scores.negative = negScores;  % negative scores

% output
output = results;
end

%% Test rcfs
% This function tests the random clustering ferns over samples.
function [output1,output2] = fun_test_rcfs(rcfs,samples)
if (nargin~=2), fun_messages('incorrect input variables','error'); end

% test the rcfs using plsa for clustering
[scores,labels] = fun_test_rcfs_plsa(rcfs,samples);

% output
output1 = scores;
output2 = labels;
end

%% Test rcfs - plsa
% This function tests the random clustering ferns using plsa.
function [output1,output2] = fun_test_rcfs_plsa(rcfs,samples)
if (nargin~=2), fun_messages('incorrect input variables','error'); end

% classifier
numFerns = size(rcfs.classifier.ferns,1);  % num ferns
numFeats = size(rcfs.classifier.ferns,2);  % num. features
numBins = 2^numFeats;  % num. histogram bins -fern outputs-
numClusters = size(rcfs.clusters.Pw_t,2);  % num. class clusters

% num. samples
numSamples = size(samples,1);

% allocate
scores = zeros(1,numSamples);  % samples scores
estLabels = zeros(1,numSamples);  % estimated sample labels

% test random ferns
for iterSample = 1:numSamples

        % variables
        score = 0;  % score
        hstm = zeros(1,numClusters);  % class distribution

        % fern output values
        values = fun_fern_outputs(samples(iterSample,:),rcfs.classifier.ferns);

        % test ferns
        for iterFern = 1:numFerns
            % fern output
            z = values(iterFern);
            % update score
            score = score + rcfs.classifier.hstms(iterFern,z);
            % visual word
            w = numBins*(iterFern-1) + z;
            % class distribution
            hstm = hstm + rcfs.clusters.Pw_t(w,:);
        end
        
        % normalize
        score = score/numFerns;

        % estimated label
        [~,label] = max(hstm);

        % samples scores and labels
        scores(1,iterSample) = score;
        labels(1,iterSample) = label;
end

% output
output1 = scores;
output2 = labels;
end

%% Classification rates
% This function computes some classification rates of the classifier over 
% samples. The function uses the sample scores -classifier confidence- with
% the sample labels to compute these classification rates. Specifically, 
% the function computes the precision, recall and f-measure plots, and the
% equal error rate (EER), that is the point where recall and precision are
% equal. The function also computes true positives, false positives and
% false negatives rates.
function output = fun_classification_rates(scores,labels)
if (nargin~=2), fun_messages('incorrect input variables','error'); end

% positive and negative indexes
posIndxs = find(labels>0);
negIndxs = find(labels<0);

% variables
eer = 0;  % equal error rate
mrt = inf;  % max. rate
thr = 0;  % classification threshold
cnt = 0;  % counter 
idx = 0;  % index

% allocate
rec = zeros(1,1000);  % recall
pre = zeros(1,1000);  % precision
fme = zeros(1,1000);  % f-measure
tps = zeros(1,1000);  % true positives
fps = zeros(1,1000);  % false positives
fns = zeros(1,1000);  % false negatives

% min. and max. threshold values
maxThr = max(scores);  % max. threshold
minThr = min(scores);  % min. threshold

% threshold step
stepThr = (maxThr-minThr)/(1000-1);

% threshold
for iterThr = maxThr:-stepThr:minThr
   
    % counter
    cnt = cnt + 1;
    
    % rates
    tp = sum(scores(posIndxs)>iterThr,2);   % true positives
    fp = sum(scores(negIndxs)>iterThr,2);   % false positives
    fn = sum(scores(posIndxs)<=iterThr,2);  % false negatives
    
    % classification rates
    r = tp/(tp + fn);  % recall
    p = tp/(tp + fp);  % precision
    f = 2*r*p/(r+p);  % f-measure
    
    % dif. recall-precision rate
    rate = abs(r-p);
    
    % best rate
    if (rate<mrt && r~=0 && p~=0)
        % update values
        mrt = rate;  % max. rate
        thr = iterThr;  % threshold
        eer = (r+p)/2;  % equal error rate
        idx = cnt;  % index
    end
    
    % save values
    tps(cnt) = tp;
    fps(cnt) = fp;
    fns(cnt) = fn;
    rec(cnt) = r;
    pre(cnt) = p;
    fme(cnt) = f;
    
end

% check
if (eer==0), fun_messages('eer value is zero','warning'); end

% classification data
data.eer = eer;  % equal error rate (EER)
data.thr = thr;  % classification threshold to achieve EER
data.idx = idx;  % EER rate index
data.rec = rec;  % recall rates
data.pre = pre;  % precision rates
data.fme = fme;  % f-measure rates
data.tps = tps;  % true positives
data.fps = fps;  % false positives
data.fns = fns;  % false negatives

% output
output = data;
end

%% Distance between positive and negative score distributions
% This function computes the distance between the positive and negative
% score distributions -gaussians-.
function output = fun_score_distributions_distance(posMiu,negMiu,posVar,negVar)
if (nargin~=4), fun_messages('incorrect input variables','error'); end

% hellinger distance
dst = fun_hellinger_distance(posMiu,negMiu,posVar,negVar);

% check empty distributions
if(posVar==0 || negVar==0), dst = 0; end

% output
output = dst;
end

%% Hellinger distance
% This function computes the hellinger distance between two gaussian
% distributions.
function output = fun_hellinger_distance(posMiu,negMiu,posVar,negVar)
if (nargin~=4), fun_messages('incorrect input variables','error'); end

% Hellinger distance -> [0,1]: 
k1 = 2*sqrt(posVar)*sqrt(negVar);
k2 = posVar + negVar;
k3 = (posMiu-negMiu)^2;
dst = 1 - sqrt(k1/k2)*exp(-0.25*k3/k2);

% output
output = dst;
end

%% confusion matrix
% This function builds the confusion matrix. Since the clustering is automatic
% and the assignment of the intra-class cluster labels in the training samples
% is at random, the confusion matrix is not necessary a diagonal matrix. Then, 
% the metric used in this work is the entropy of the confusion matrix. Low 
% entropy values correspond to "good" clustering results. The confusion matrix
% is only computed for positive samples.
function [output1,output2] = fun_confusion_matrix(labels,estLabels,numClusters)
if (nargin~=3), fun_messages('incorrect input variables','error'); end

% num. samples
numSamples = size(labels,1);

% allocate
mat = zeros(numClusters,numClusters);

% confusion matrix
for iterSample = 1:numSamples
	% labels
	a = labels(iterSample);  % true label
	b = estLabels(iterSample);  % estimated label
	% update confusion matrix
	if (a>0&&b>0), mat(a,b) = mat(a,b) + 1; end
end

% entropy value
ent = 0;
for iter = 1:numClusters
	vec = mat(:,iter)';
	vec = vec + eps;
	vec = vec/sum(vec(:));
	ent = ent + sum(vec.*log2(vec),2);
end
ent = -1*ent/numClusters;

% output
output1 = mat;
output2 = ent;
end
